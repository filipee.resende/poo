
using System;
using POO.NotificationContext;

namespace POO.SharedConetxt
{
    public abstract class Base : Notifiable
    {
        public Base()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
    }
}