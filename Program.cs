﻿using System;
using System.Collections.Generic;
using System.Linq;
using POO.ContentContext;
using POO.Subscriptconetxt;

namespace POO
{
    class Program
    {
        static void Main(string[] args)
        {
            var articles = new List<Article>();
            articles.Add(new Article("Artigo sobre oop", "orientacao-objetos"));
            articles.Add(new Article("Artigo sobre c#", "csharp"));
            articles.Add(new Article("Artigo sobre .net", "dotnet"));

            // foreach (var article in articles)
            // {
            //     Console.WriteLine(article.Id);
            //     Console.WriteLine(article.Title);
            //     Console.WriteLine(article.Url);
            // }
            var courses = new List<Course>();
            var courseOOP = new Course("fundamentos OOP", "fundamento-OOP");
            var courseCSHARP = new Course("C#", "CSHARP");
            var courseASPNET = new Course("ASPNET", ".ASPNET");

            courses.Add(courseOOP);
            courses.Add(courseCSHARP);
            courses.Add(courseASPNET);

            var careers = new List<Career>();
            var careerDotnet = new Career("Especialista .net", "Especialista-dotnet");
            var careerItem = new CareerItem(1, "comece por aqui", "", null);
            var careerItem1 = new CareerItem(3, "aprenda .net", "", courseASPNET);
            var careerItem2 = new CareerItem(2, "aprenda c#", "", courseOOP);

            careerDotnet.Items.Add(careerItem);
            careerDotnet.Items.Add(careerItem1);
            careerDotnet.Items.Add(careerItem2);

            careers.Add(careerDotnet);


            foreach (var career in careers)
            {
                Console.WriteLine(career.Title);

                foreach (var item in career.Items.OrderBy(x => x.Ordem))
                {
                    Console.WriteLine($"{item.Ordem} - {item.Title}");
                    Console.WriteLine(item.Course?.Title);
                    Console.WriteLine(item.Course?.Level);
                    foreach (var notification in item.Notifications)
                    {
                        Console.WriteLine($"{notification.Property}     -     {notification.Message}");
                    }

                }

                var payPalSubscription = new PayPalSubscription();

                var student = new Student();

                student.CreatSubscription(payPalSubscription);
            }
        }
    }
}
